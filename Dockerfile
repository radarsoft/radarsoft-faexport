FROM docker.io/oven/bun:0.6.5
EXPOSE 8101
WORKDIR /faexport
COPY . .
RUN bun install
ENTRYPOINT ["bun", "index.ts"]

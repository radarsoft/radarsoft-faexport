import { Request } from 'express'
import { load }  from 'cheerio'
import { OnlineStats } from './types'

export const parseStatus: (req?: Request) => Promise<OnlineStats> = async (req?: Request) => {
  let FA_COOKIE = Bun.env.FA_COOKIE

  if (req) {
    FA_COOKIE = req.header('fa-cookie')
  }

  const faResult = !FA_COOKIE ? await fetch('https://furaffinity.net') : await fetch('https://furaffinity.net', {
    headers: {
      cookie: FA_COOKIE
    }
  });
  
  const resultBlob = await faResult.blob()
  const resultHtml = await resultBlob.text()

  const $ = load(resultHtml)
  const theme = $('body').attr('data-static-path')?.split('/').pop()

  let selector
  switch(theme) {
    case 'beta':
      selector = '.online-stats'
      break
    case 'classic':
      selector = '.footer center'
      break
    default:
      throw new Error("Unsupported theme!")
  }

  const onlineStats = $(selector)
  if (!onlineStats) {
    return {
      online: 0,
      guests: 0,
      registered: 0,
      other: 0
    }
  }

  const onlineStatsText = onlineStats.text().replace(/(\r\n|\n|\r)/gm, "");
  const statsRegex = /\s+(?<online>\d+)\s+Users\sonline+[^0-9]+(?<guests>\d+)\sguests[^0-9]+(?<registered>\d+)\sregistered[^0-9]+(?<other>\d+)\sother.+/m
  const statsParsed = onlineStatsText.match(statsRegex)

  return {
    online: parseInt(statsParsed?.groups?.online || '0'),
    guests: parseInt(statsParsed?.groups?.guests || '0'),
    registered: parseInt(statsParsed?.groups?.registered || '0'),
    other: parseInt(statsParsed?.groups?.other || '0')
  }
}

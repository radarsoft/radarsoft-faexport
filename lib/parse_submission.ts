import { Request } from "express";
import { Submission } from "./types";
import { load } from "cheerio";


export const parseSubmission: (req: Request, favorite?: boolean) => Promise<Submission | string> = async (req: Request, favorite: boolean = false) => {
  let link = `https://furaffinity.net/view/${req.params.id}`
  let fav_status
  let fav_key

  if (favorite) {
    fav_status = req.body.fav_status
    fav_key = req.body.fav_key

    if (fav_status === undefined || fav_status === null || !fav_key) {
      throw new Error('Malformed request body')
    }

    link = `https://furaffinity.net/${fav_status ? 'fav' : 'unfav'}/${req.params.id}/?key=${fav_key}`
  }

  const FA_COOKIE = req.header('fa-cookie')

  if (favorite && !FA_COOKIE) {
    throw new Error('Favoriting requires set "fa-cookie"!')
  }

  const faResult = !FA_COOKIE ? await fetch(link) : await fetch(link, {
    headers: {
      cookie: FA_COOKIE
    }
  })

  const resultBlob = await faResult.blob()
  const resultHtml = await resultBlob.text()

  const $ = load(resultHtml)
  
  if ($.text().includes('The submission you are trying to find is not in our database.')) {
    throw new Error('not-found')
  }

  const theme = $('body')?.attr('data-static-path')?.split('/').pop()

  const selectors: any = {
    date: '.popup_date',
    submissionImage: '#submissionImg'
  }

  let stats: {
    category?: string,
    theme?: string,
    species?: string,
    gender?: string,
    favorites?: number,
    comments?: number,
    views?: number,
    resolution?: string,
    rating?: string
  } = {}

  switch (theme) {
    case 'beta':
      selectors.title = '.submission-title h2'
      selectors.description = '.submission-description'
      selectors.name = '.submission-id-sub-container a'
      selectors.avatar = '.submission-id-avatar img'
      selectors.keywords = '.tags-row .tags a'

      const infoContainer = $('.info.text')
      const statsContainer = $('.stats-container.text')

      stats = {
        category: infoContainer?.find('.category-name')?.text()?.trim(),
        theme: infoContainer?.find('.type-name')?.text()?.trim(),
        species: infoContainer?.find('> div:nth-child(2) span')?.text()?.trim(),
        gender: infoContainer?.find('> div:nth-child(3) span')?.text()?.trim(),
        favorites: parseInt(statsContainer?.find('.favorites .font-large')?.text() || '0'),
        comments: parseInt(statsContainer?.find('.comments .font-large')?.text() || '0'),
        views: parseInt(statsContainer?.find('.views .font-large')?.text() || '0'),
        resolution: infoContainer?.find('div:nth-child(4) span')?.text()?.replaceAll(' ', ''),
        rating: statsContainer?.find('.rating .font-large')?.text()?.trim()
      }

      break
    case 'classic':
      selectors.title = '.classic-submission-title.information h2'
      selectors.description = '.maintable .maintable tr:nth-child(2) td'
      selectors.name = '.classic-submission-title.information a'
      selectors.avatar = '.classic-submissiont-title.avatar img'
      selectors.keywords = '.maintable .maintable tr td:nth-child(2) td.stats-container #keywords a'

      const statsContainerClassic = $('.maintable .maintable tr td:nth-child(2) td.stats-container')
      const statsText = statsContainerClassic?.text()?.replace(/(\r\n|\n|\r)/gm, "").trim()
      const statsParsingRegex = /.+Category:\s(?<category>\w+\s*\(*\w*\)*).+Theme:\s(?<theme>\w+).+Species:\s(?<species>\w+\s*\/*\s*\w+).+Gender:\s(?<gender>\w+).+Favorites:\s+(?<favorites>\d+).+\sComments:\s+(?<comments>\d+).+\sViews:\s+(?<views>\d+).+\sResolution:\s+(?<resolution>\d+x\d+).+/m
      const parsedStats = statsText?.match(statsParsingRegex)
      stats = {
        category: parsedStats?.groups?.category,
        theme: parsedStats?.groups?.theme,
        species: parsedStats?.groups?.species,
        gender: parsedStats?.groups?.gender,
        favorites: parseInt(parsedStats?.groups?.favorites || '0'),
        comments: parseInt(parsedStats?.groups?.comments || '0'),
        views: parseInt(parsedStats?.groups?.views || '0'),
        resolution: parsedStats?.groups?.resolution,
        rating: statsContainerClassic?.children('img')?.attr('alt')?.split(' ')[0]
      }

      break
    default:
      throw new Error('Unsupported theme')
  }

  let favStatus, favKey

  if (FA_COOKIE) {
    let favAnchor = $(`a[href^="/fav/${req.params.id}/?key="]`)
    let unfavAnchor = $(`a[href^="/unfav/${req.params.id}/?key="]`)

    if (favAnchor.length) {
      favStatus = false
      const favUrl = new URL(`https://furaffinity.net${favAnchor.attr('href')}`)
      favKey = favUrl.searchParams.get('key')
    } else if (unfavAnchor.length) {
      favStatus = true
      const unfavUrl = new URL(`https://furaffinity.net${unfavAnchor.attr('href')}`)
      favKey = unfavUrl.searchParams.get('key')
    }

		console.log(favKey)
  }

  return {
    title: $(selectors.title)?.text().trim(),
    description: $(selectors.description)?.html()?.trim(),
    description_body: $(selectors.description)?.html()?.trim(),
    name: $(selectors.name)?.text().trim(),
    profile: `https://furaffinity.net${$(selectors.name)?.attr('href')}`,
    profile_name: $(selectors.name)?.attr('href')?.split('/')?.filter(val => !!val)?.pop(),
    avatar: `https:${$(selectors.avatar)?.attr('src')}`,
    link,
    posted: $(selectors.date)?.attr('title'),
    posted_at: new Date(Date.parse($('.popup_date')?.attr('title') || '')),
    download: `https:${$(selectors.submissionImage)?.attr('data-fullview-src')}`,
    full: `https:${$(selectors.submissionImage)?.attr('data-fullview-src')}`,
    thumbnail: `https:${$(selectors.submissionImage)?.attr('data-preview-src')}`,
    ...stats,
    keywords: $(selectors.keywords).toArray().map(el => $(el).text()).filter(t => t.length > 0),
    fav_status: favStatus,
    fav_key: favKey
  }
}

export type OnlineStats = {
  online: number,
  guests: number,
  registered: number,
  other: number
}

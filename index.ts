import express, { Request, Response, NextFunction } from 'express'
import { Gauge, register } from 'prom-client'
import { parseStatus, parseSubmission } from './lib'
import { Counter } from 'prom-client'

const app = express()
app.use(express.json())
app.use("/", (req, res, next) => {
  console.log(req.method, req.originalUrl)
  console.log(req.headers)
  console.log(req.body)
  next()
})

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err)
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

const port = parseInt(Bun.env.PORT || '8101')

const userOnlineGauge = new Gauge({
  name: 'faexport_users_online',
  help: 'Number of users online',
  labelNames: [
    'user_kind'
  ]
})

const scraperRequestsCounter = new Counter({
  name: 'faexport_requests',
  help: 'Number of requests to each endpoint of the faexport API',
  labelNames: [
    'endpoint'
  ]
})

const processStatus = async (req?: Request) => {
  scraperRequestsCounter.inc({ 'endpoint': 'status' })
  const response = await parseStatus(req)
  userOnlineGauge.set({ 'user_kind': 'total' }, response.online)
  userOnlineGauge.set({ 'user_kind': 'registered' }, response.registered)
  userOnlineGauge.set({ 'user_kind': 'guests' }, response.guests)
  userOnlineGauge.set({ 'user_kind': 'other' }, response.other)

  return response
}

app.get('/metrics', async (req, res) => {
  scraperRequestsCounter.inc({ 'endpoint': 'metrics' })
  res.type('text/plain').send(await register.metrics())
})

app.get('/status', async (req, res) => {
  try {
    const response = await processStatus(req)

    res.json(response)
  } catch (e) {
    res.status(500).send(e)
  }
})

app.post('/submission/:id/favorite', async (req, res) => {
  scraperRequestsCounter.inc({ 'endpoint': 'submission/favorite' })
  try {
    const response = await parseSubmission(req, true)
    console.debug(JSON.stringify(response, null, 2))

    if (typeof response === 'string') {
      res.send(response)
      return
    }

    res.json(response)
  } catch (e: any) {
    switch(e.message) {
      case 'not-found':
        res.status(404).send()
        break
      default: 
        res.status(500).send(e)
        break
    }
  }
})

app.get('/submission/:id', async (req, res) => {
  scraperRequestsCounter.inc({ 'endpoint': 'submission' })
  try {
    const response = await parseSubmission(req)

    if (typeof response === 'string') {
      res.send(response)
      return
    }

    res.json(response)
  } catch (e: any) {
    switch(e.message) {
      case 'not-found':
        res.status(404).send()
        break
      default: 
        res.status(500).send(e)
        break
    }
  }
})

app.get('/', (req, res) => {
  scraperRequestsCounter.inc({ 'endpoint': 'index' })
  res.send("Hello from express on Bun!")
})

app.listen(port, () => {
  console.info(`Listening on port ${port}`)
  processStatus()
  setInterval(processStatus, 60000)
})